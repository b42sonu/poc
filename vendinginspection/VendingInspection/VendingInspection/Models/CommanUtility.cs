﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VendingInspection.Models
{
    public class CommanUtility
    {
        public enum ErrorType
        {
            Error,
            Warning,
            Information
        }

        public static string GetMessage(ErrorType errorType, string message)
        {
            string formattedMessage = string.Empty;
            switch (errorType)
            {
                case ErrorType.Warning:
                    formattedMessage = "<span style='color:yellow;font-weight: bold'>" + message + "</span>";
                    break;
                case ErrorType.Error:
                    formattedMessage = "<span style='color:red;font-weight: bold'>" + message + "</span>";
                    break;
                case ErrorType.Information:
                    formattedMessage = "<span style='color:green;font-weight: bold'>" + message + "</span>";
                    break;
            }
            return formattedMessage;
        }
    }
}