﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VendingInspection.ViewModels
{
    public class UserJobInformationViewModel
    {
        public Int64 JobId { get; set; }
        public Int32 JobTypeId { get; set; }
        public String BarCode { get; set; }
        public String CustomerName { get; set; }
        public String CustomerAddress { get; set; }
        public String ContactPerson { get; set; }
        public String ContactNumber { get; set; }
        public String MachineLocation { get; set; }
        public String Lane { get; set; }
        public String Model { get; set; }
        public String AcOrDc { get; set; }
        public String WaterSource { get; set; }
        public String ManufacturerName { get; set; }
        public String ManufacturerYear { get; set; }
        public String Branding { get; set; }
    }
}