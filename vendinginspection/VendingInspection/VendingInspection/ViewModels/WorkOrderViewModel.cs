﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VendingInspection.ViewModels
{
    public class WorkOrderViewModel
    {
        public Int32 CompanyId { get; set; }
        public Int64 JobId { get; set; }
        public string Value { get; set; }
        public Int64 WorkOrderId { get; set; }
        public Int32 WorkOrderParameterId { get; set; }
    }
}