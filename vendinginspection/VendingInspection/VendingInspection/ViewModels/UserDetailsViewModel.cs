﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VendingInspection.ViewModels
{
    public class UserDetailsViewModel
    {
        public Int64 UserDetailId { get; set; }
        public Int64 UserId { get; set; }
        public String UserName { get; set; }
        public String Password { get; set; }
        public String City { get; set;}
        public Int32 CompanyId { get; set; }
        public String State { get; set; }
        public String ZipCode { get; set; }
    }
}