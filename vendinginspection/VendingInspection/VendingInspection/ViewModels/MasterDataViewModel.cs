﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VendingInspection.Models;

namespace VendingInspection.ViewModels
{
    public class MasterDataViewModel
    {
        public List<Object> Brands { get; set; }
        public List<Object> Manufacturers { get; set; }
        public List<Object> RSDetails { get; set; }
        public List<Object> RouteCauses { get; set; }
        public List<Object> ComplaintCategories { get; set; }
        public List<Object> Models { get; set; }
        public List<Object> WaterSources { get; set; }
        public List<Object> Lanes { get; set; }
        public List<Object> ManufacturerYears { get; set; }
    }
}