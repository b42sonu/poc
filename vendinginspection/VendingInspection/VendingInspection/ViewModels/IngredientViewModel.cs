﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VendingInspection.ViewModels
{
    public class IngredientViewModel
    {
        public Int32 IngredientId { get; set; }
        public String IngredientName { get; set; }
        public Int32 Water { get; set; }
        public Int32 Premix { get; set; }
        public Int32 Temp { get; set; }
        public Int32 Counter { get; set; }
    }
}