﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VendingInspection.ViewModels
{
    public class ComplaintDetailViewModel
    {
        public int ComplaintCategoryId;
        public String ComplaintName;
        public int RouteCauseId;
        public String RouteCauseName;
    }

}