﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VendingInspection.ViewModels
{
    public class InventoryItemViewModel
    {
        public Int32 ItemId { get; set; }
        public string ItemName { get; set; }
        public Int32 Quantity { get; set; }
    }
}