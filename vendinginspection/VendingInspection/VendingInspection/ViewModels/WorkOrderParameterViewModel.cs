﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VendingInspection.ViewModels
{
    public class WorkOrderParameterViewModel
    {
        public String ParameterName { get; set; }
        public String ParameterType { get; set; }
        public Int32 WorkOrderParameterId { get; set; }
    }
}