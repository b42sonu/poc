﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VendingInspection.Models;

namespace VendingInspection.ViewModels
{
    public class WorkOrderDetailViewModel
    {
        public int JobId;
        public int Userid;
        public int CompanyId;
        public String BarCode;
        public int RSId;
        public string Lane;
        public String Model;
        public String AcOrDc;
        public String WaterSource;
        public String ManufecturerName;
        public String ManufecturerYear;
        public String Branding;
        public String OverAllBodyCondition;
        public Boolean IsSparePartsUsed;
        public int JobTypeId;
        public Int64 TechnicianId;
        public string JobStartLocation;
        public string JobEndLocation;
        public DateTime JobStartDateTime;
        public string Remarks;
        public List<InventoryItemViewModel> SparePartUsed;
        public List<IngredientViewModel> IngredientDetail;
        public List<string> Pictures;
        public string Signature;
        public string ContactNo;
        public List<WorkOrder> WorkOrderParameterDetail;
        public List<ComplaintDetailViewModel> ComplaintDetail;
    }
}