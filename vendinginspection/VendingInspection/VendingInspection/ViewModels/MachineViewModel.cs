﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VendingInspection.ViewModels
{
    public class MachineViewModel
    {
        public Int64 MachineId { get; set; }
        public string TechnicianName { get; set; }
        public string BarCode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string Active { get; set; }
        public string RSName { get; set; }
    }
}