﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data;

namespace VendingInspection.Controllers
{
    public class ImportController : Controller
    {
        //
        // GET: /Import/

        public ActionResult Import()
        {
            return View();
        }

        public ActionResult ImportMachine()
        {
            if (Request.Files["FileUpload1"].ContentLength > 0)
            {
                string extension = System.IO.Path.GetExtension(Request.Files["FileUpload1"].FileName);
                string path1 = string.Format("{0}/{1}", Server.MapPath("~/Content/UploadedFiles"), Request.Files["FileUpload1"].FileName);
                if (System.IO.File.Exists(path1))
                    System.IO.File.Delete(path1);

                Request.Files["FileUpload1"].SaveAs(path1);
                string sqlConnectionString = @"Data Source=SLW10236\MSSQLSERVER2012;Initial Catalog=VendingCare;user id=sa;password=span@1234;";

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[30] { 
                    new DataColumn("Serial", typeof(string)),
            new DataColumn("Region", typeof(string)),
            new DataColumn("RSName",typeof(string)), 
            new DataColumn("BarCode",typeof(string)) ,
            new DataColumn("InternalId",typeof(string)) ,
            new DataColumn("CustomerId",typeof(string)) ,
            new DataColumn("CustomerName",typeof(string)) ,
            new DataColumn("CustomerAddress",typeof(string)) ,
            new DataColumn("AreaCode",typeof(string)) ,
            new DataColumn("City",typeof(string)) ,
            new DataColumn("State",typeof(string)) ,
            new DataColumn("PinCode",typeof(string)) ,
            new DataColumn("IsActive",typeof(string)) ,
            new DataColumn("MachineLocation",typeof(string)) ,
            new DataColumn("ContactPerson",typeof(string)) ,
            new DataColumn("ContactNumber",typeof(string)) ,
            new DataColumn("NumberOfMachine",typeof(string)) ,
            new DataColumn("MachineStatus",typeof(string)) ,
            new DataColumn("Category",typeof(string)) ,
            new DataColumn("TypeOfAccount",typeof(string)) ,
            new DataColumn("ClassOfAccount",typeof(string)) ,
            new DataColumn("Lane",typeof(string)) ,
            new DataColumn("Model",typeof(string)) ,
            new DataColumn("AcOrDc",typeof(string)) ,
            new DataColumn("WaterSource",typeof(string)) ,
            new DataColumn("ManufecturerName",typeof(string)) ,
            new DataColumn("ManufecturerYear",typeof(string)) ,
            new DataColumn("Branding",typeof(string)) ,
            new DataColumn("TechnicianName",typeof(string)) ,
            new DataColumn("Remarks",typeof(string)) ,
                });


                string csvData = System.IO.File.ReadAllText(path1);
                foreach (string row in csvData.Split('\n'))
                {
                    if (!string.IsNullOrEmpty(row))
                    {
                        dt.Rows.Add();
                        int i = 0;
                        string[] columns= row.Split('|');
                        for (i = 0; i < columns.Length;i++)
                        {
                            dt.Rows[dt.Rows.Count - 1][i] = columns[i];
                        }
                    }
                }

                SqlBulkCopy sqlBulk = new SqlBulkCopy(sqlConnectionString);
                //Give your Destination table name
                sqlBulk.DestinationTableName = "Machine";
                sqlBulk.WriteToServer(dt);
            }

            return RedirectToAction("Import");
        }

        public ActionResult ImportSpare()
        {
            if (Request.Files["FileUpload2"].ContentLength > 0)
            {
                string extension = System.IO.Path.GetExtension(Request.Files["FileUpload2"].FileName);
                string path1 = string.Format("{0}/{1}", Server.MapPath("~/Content/UploadedFiles"), Request.Files["FileUpload2"].FileName);
                if (System.IO.File.Exists(path1))
                    System.IO.File.Delete(path1);

                Request.Files["FileUpload2"].SaveAs(path1);
                string sqlConnectionString = @"Data Source=SLW10236\MSSQLSERVER2012;Initial Catalog=VendingCare;user id=sa;password=span@1234;";

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[8] { 
                    new DataColumn("Serial", typeof(string)),
            new DataColumn("ServiceCenterCode", typeof(string)),
            new DataColumn("ItemCode",typeof(string)), 
            new DataColumn("ItemName",typeof(string)) ,
            new DataColumn("SipplierName",typeof(string)) ,
            new DataColumn("MachineDetails",typeof(string)) ,
            new DataColumn("Quantity",typeof(string)) ,
            new DataColumn("Rate",typeof(string))
                });


                string csvData = System.IO.File.ReadAllText(path1);
                foreach (string row in csvData.Split('\n'))
                {
                    if (!string.IsNullOrEmpty(row))
                    {
                        dt.Rows.Add();
                        int i = 0;
                        string[] columns = row.Split('|');
                        for (i = 0; i < columns.Length; i++)
                        {
                            dt.Rows[dt.Rows.Count - 1][i+1] = columns[i];
                        }
                    }
                }

                SqlBulkCopy sqlBulk = new SqlBulkCopy(sqlConnectionString);
                //Give your Destination table name
                sqlBulk.DestinationTableName = "InventoryItem";
                sqlBulk.WriteToServer(dt);
            }

            return RedirectToAction("Import");
        }


        public ActionResult ImportRS()
        {
            if (Request.Files["FileUpload3"].ContentLength > 0)
            {
                string extension = System.IO.Path.GetExtension(Request.Files["FileUpload3"].FileName);
                string path1 = string.Format("{0}/{1}", Server.MapPath("~/Content/UploadedFiles"), Request.Files["FileUpload3"].FileName);
                if (System.IO.File.Exists(path1))
                    System.IO.File.Delete(path1);

                Request.Files["FileUpload3"].SaveAs(path1);
                string sqlConnectionString = @"Data Source=SLW10236\MSSQLSERVER2012;Initial Catalog=VendingCare;user id=sa;password=span@1234;";

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[6] { 
                    new DataColumn("Serial", typeof(string)),
                    new DataColumn("RSOrSubRS", typeof(string)),
            new DataColumn("RSName", typeof(string)),
            new DataColumn("Address",typeof(string)), 
            new DataColumn("City",typeof(string)) ,
            new DataColumn("Region",typeof(string)) 
                });


                string csvData = System.IO.File.ReadAllText(path1);
                foreach (string row in csvData.Split('\n'))
                {
                    if (!string.IsNullOrEmpty(row))
                    {
                        dt.Rows.Add();
                        int i = 0;
                        string[] columns = row.Split('|');
                        for (i = 0; i < columns.Length; i++)
                        {
                            dt.Rows[dt.Rows.Count - 1][i+1] = columns[i];
                        }
                    }
                }

                SqlBulkCopy sqlBulk = new SqlBulkCopy(sqlConnectionString);
                //Give your Destination table name
                sqlBulk.DestinationTableName = "RS";
                sqlBulk.WriteToServer(dt);
            }

            return RedirectToAction("Import");
        }


    }
}
