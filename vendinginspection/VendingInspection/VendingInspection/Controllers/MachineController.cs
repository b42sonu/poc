﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VendingInspection.Models;
using VendingInspection.ViewModels;
using System.Web.Script.Serialization;

namespace VendingInspection.Controllers
{
    public class MachineController : Controller
    {
        private VendingCareEntities db = new VendingCareEntities();

        #region Method and Functions related to Web Service

        


        #endregion



        [HttpPost]
        public JsonResult GetMachineDetailsFromBarcode(string barcode)
        {
            try
            {
                var machine = db.GetMachineFromBarCode(barcode).SingleOrDefault();
               
                if (machine != null)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    string result = serializer.Serialize(machine);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }

            return null;
        }


        //
        // GET: /Machine/

        public ActionResult Index()
        {
            IEnumerable<MachineViewModel> model = null;
            model = (IEnumerable<MachineViewModel>)(from machine in db.Machines
                                                     join machinetechnicianLink in db.MachineTechnicianLinks
                                                    on machine.MachineId equals machinetechnicianLink.MachineId
                                                    join user in db.UserDetails on machinetechnicianLink.UserDetailId equals user.UserDetailId
                                                    join machinerslink in db.MachineRSLinks
                                                    on machine.MachineId equals machinerslink.MachineId
                                                    join rs in db.RSDetails on  machinerslink.RSId equals rs.RSId
                                                    select new
                                                    {
                                                        MachineId = machinetechnicianLink.MachineId,
                                                        TechnicianName = user.FirstName + " " + user.LastName,
                                                        BarCode = machine.Barcode,
                                                        CustomerName = machine.CustomerName,
                                                        Active = machine.IsActive,
                                                        RSName = rs.RSName,
                                                        CustomerAddress = machine.CustomerAddress
                                                    }).ToList().ToNonAnonymousList(typeof(MachineViewModel));

            
            return View(model);
        }

        //
        // GET: /Machine/Details/5

        public ActionResult Details(int id = 0)
        {
            Machine machine = db.Machines.Single(m => m.MachineId == id);
            if (machine == null)
            {
                return HttpNotFound();
            }
            return View(machine);
        }

        //
        // GET: /Machine/Create

        public ActionResult Create()
        {
            ViewBag.Technicians = new SelectList(db.GetAllTechnicians(), "UserDetailId", "FirstName");
            ViewBag.Brands = new SelectList(db.Brands, "BrandId", "BrandName");
            ViewBag.Models = new SelectList(db.Models, "ModelId", "ModelName");
            ViewBag.WaterSources = new SelectList(db.WaterSources, "WaterSourceId", "WaterSourceName");
            ViewBag.Manufacturers = new SelectList(db.Manufacturers, "MfgrId", "MfgrName");
            ViewBag.AccountClasses = new SelectList(db.AccountClasses, "AccountClassId", "AccountClassName");
            ViewBag.RS = new SelectList(db.RSDetails, "RSId", "RSName");
            return View();
        }

        //
        // POST: /Machine/Create

        [HttpPost]
        public ActionResult Create(Machine machine, Int64 Technician, Int32 RS)
        {
            if (ModelState.IsValid)
            {
                db.Machines.AddObject(machine);
                db.SaveChanges();

                var machineTechLink = new MachineTechnicianLink();
                machineTechLink.MachineId = machine.MachineId;
                machineTechLink.UserDetailId = Technician;
                db.MachineTechnicianLinks.AddObject(machineTechLink);
                db.SaveChanges();

                var machineRSLink = new MachineRSLink();
                machineRSLink.MachineId = machine.MachineId;
                machineRSLink.RSId = RS;
                db.MachineRSLinks.AddObject(machineRSLink);
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(machine);
        }

        //
        // GET: /Machine/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Machine machine = db.Machines.Single(m => m.MachineId == id);

            var technicianId = db.MachineTechnicianLinks.Where(m => m.MachineId == machine.MachineId).SingleOrDefault().UserDetailId;
            var rsId = db.MachineRSLinks.Where(m => m.MachineId == machine.MachineId).SingleOrDefault().RSId;

            ViewBag.Technicians = new SelectList(db.GetAllTechnicians(),"UserDetailId", "FirstName", technicianId);
            ViewBag.Brands = new SelectList(db.Brands, "BrandName", "BrandName");
            ViewBag.Models = new SelectList(db.Models, "ModelName", "ModelName");
            ViewBag.WaterSources = new SelectList(db.WaterSources, "WaterSourceName", "WaterSourceName");
            ViewBag.Manufacturers = new SelectList(db.Manufacturers, "MfgrName", "MfgrName");
            ViewBag.AccountClasses = new SelectList(db.AccountClasses, "AccountClassName", "AccountClassName");
            ViewBag.RSDetails = new SelectList(db.RSDetails,"RSId", "RSName", rsId);
            if (machine == null)
            {
                return HttpNotFound();
            }
            return View(machine);
        }

        //
        // POST: /Machine/Edit/5

        [HttpPost]
        public ActionResult Edit(Machine machine, Int64 Technician, Int32 RSName)
        {
            if (ModelState.IsValid)
            {
                db.Machines.Attach(machine);
                db.ObjectStateManager.ChangeObjectState(machine, EntityState.Modified);
                db.SaveChanges();

                var machineTechLink = db.MachineTechnicianLinks.Where(m => m.MachineId == machine.MachineId).SingleOrDefault();
                machineTechLink.UserDetailId = Technician;
                db.ObjectStateManager.ChangeObjectState(machineTechLink, EntityState.Modified);
                db.SaveChanges();

                var machineRSLink = db.MachineRSLinks.Where(m => m.MachineId == machine.MachineId).SingleOrDefault();
                machineRSLink.RSId = RSName;
                db.ObjectStateManager.ChangeObjectState(machineRSLink, EntityState.Modified);
                db.SaveChanges();


                return RedirectToAction("Index");
            }
            return View(machine);
        }

        //
        // GET: /Machine/Delete/5

        public ActionResult Delete(int id = 0)
        {
            var machineTechnicianLink = db.MachineTechnicianLinks.Single(machineTechLink => machineTechLink.MachineId == id);
            db.MachineTechnicianLinks.DeleteObject(machineTechnicianLink);
            db.SaveChanges();
            var macRSLink = db.MachineRSLinks.Single(machineRSLink => machineRSLink.MachineId == id);
            db.MachineRSLinks.DeleteObject(macRSLink);
            db.SaveChanges();
            Machine machine = db.Machines.Single(m => m.MachineId == id);
            db.Machines.DeleteObject(machine);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}