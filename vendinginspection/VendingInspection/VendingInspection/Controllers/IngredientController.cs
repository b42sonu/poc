﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VendingInspection.Models;
using VendingInspection.ViewModels;
using System.Web.Script.Serialization;

namespace VendingInspection.Controllers
{
    public class IngredientController : Controller
    {
        private VendingCareEntities db = new VendingCareEntities();


        #region Method and functions related to web service

        [HttpGet]
        public ActionResult GetIngredients()
        {
            try
            {

                List<Ingredient> ingredientItems = db.Ingredients.ToList();
                List<IngredientViewModel> ingredients = new List<IngredientViewModel>();
                if (ModelState.IsValid)
                {
                    ingredientItems.ForEach(ingredientItem => ingredients.Add(new IngredientViewModel()
                    {
                        IngredientId = ingredientItem.IngredientId,
                        IngredientName = ingredientItem.IngredientName,
                        Water = 0,
                        Premix = 0,
                        Temp = 0,
                        Counter = 0
                    }));

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    string result = serializer.Serialize(ingredients);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }

            return null;
        }



        #endregion



        //
        // GET: /Ingredient/

        public ActionResult Index()
        {
            return View(db.Ingredients.ToList());
        }

        //
        // GET: /Ingredient/Details/5

        public ActionResult Details(int id = 0)
        {
            Ingredient ingredient = db.Ingredients.Single(i => i.IngredientId == id);
            if (ingredient == null)
            {
                return HttpNotFound();
            }
            return View(ingredient);
        }

        //
        // GET: /Ingredient/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Ingredient/Create

        [HttpPost]
        public ActionResult Create(Ingredient ingredient)
        {
            if (ModelState.IsValid)
            {
                db.Ingredients.AddObject(ingredient);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(ingredient);
        }

        //
        // GET: /Ingredient/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Ingredient ingredient = db.Ingredients.Single(i => i.IngredientId == id);
            if (ingredient == null)
            {
                return HttpNotFound();
            }
            return View(ingredient);
        }

        //
        // POST: /Ingredient/Edit/5

        [HttpPost]
        public ActionResult Edit(Ingredient ingredient)
        {
            if (ModelState.IsValid)
            {
                db.Ingredients.Attach(ingredient);
                db.ObjectStateManager.ChangeObjectState(ingredient, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ingredient);
        }

        //
        // GET: /Ingredient/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Ingredient ingredient = db.Ingredients.Single(i => i.IngredientId == id);
            if (ingredient == null)
            {
                return HttpNotFound();
            }
            return View(ingredient);
        }

        //
        // POST: /Ingredient/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Ingredient ingredient = db.Ingredients.Single(i => i.IngredientId == id);
            db.Ingredients.DeleteObject(ingredient);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}