﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VendingInspection.Models;
using VendingInspection.ViewModels;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace VendingInspection.Controllers
{
    public class WorkOrderController : Controller
    {
        private VendingCareEntities db = new VendingCareEntities();

        #region Method and Functions related to web service
        // Get work order parameters.
        [HttpGet]
        public ActionResult GetWorkOrderParameters()
        {
            try
            {
                List<WorkOrderParameter> workOrderParams = db.GetWorkOrderParameters().ToList();
                List<WorkOrderParameterViewModel> workOrderParameters = new List<WorkOrderParameterViewModel>();
                if (ModelState.IsValid && workOrderParams != null)
                {
                    workOrderParams.ForEach(workOrderParameter =>
                        {
                            workOrderParameters.Add(ModelToViewModel(workOrderParameter));
                        });

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    string result = serializer.Serialize(workOrderParameters);

                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        // Save work order.
        [HttpPost]
        public ActionResult SaveWorkOrder(string value)
        {
            try
            {
                var data = (JObject)JsonConvert.DeserializeObject(value);
                Job objJob = null;
                var workOrderDetail = data["WorkOrderDetail"].ToObject<WorkOrderDetailViewModel>();

                if (workOrderDetail.JobId > 0)
                {
                    objJob = db.Jobs.Single(j => j.JobId == workOrderDetail.JobId);
                    objJob.JobFinishDate = DateTime.Now;
                    objJob.Status = 1;
                    objJob.JobEndLocation = workOrderDetail.JobEndLocation;
                    objJob.Remarks = workOrderDetail.Remarks;
                    objJob.CustomerSignature = Convert.FromBase64String(workOrderDetail.Signature);
                    objJob.CustomerContactNo = workOrderDetail.ContactNo;
                    db.ObjectStateManager.ChangeObjectState(objJob, EntityState.Modified);
                    db.SaveChanges();

                    //Save work order parameter details
                    SaveWorkOrderParameters(workOrderDetail, objJob.JobId);

                    //Save spare part details
                    SaveWorkOrderSpareDetails(workOrderDetail, objJob.JobId);

                    //Save ingredient details
                    SaveWorkOrderIngredientDetails(workOrderDetail, objJob.JobId);

                    //Save job pictures
                    SaveWorkOrderPictures(workOrderDetail, objJob.JobId);

                    //Save Complaint Detail
                    SaveComplaintDetail(workOrderDetail, objJob.JobId);
                }
                else
                {
                    objJob = new Job();
                    objJob.CompanyId = workOrderDetail.CompanyId;
                    objJob.BarCode = workOrderDetail.BarCode;
                    objJob.JobCreationDate = DateTime.Now;
                    objJob.JobStartDate = (DateTime)workOrderDetail.JobStartDateTime;
                    objJob.JobFinishDate = DateTime.Now;
                    objJob.Status = 1;
                    objJob.Technician = workOrderDetail.TechnicianId;
                    objJob.JobTypeId = workOrderDetail.JobTypeId;
                    objJob.JobStartLocation = workOrderDetail.JobStartLocation;
                    objJob.JobEndLocation = workOrderDetail.JobEndLocation;
                    objJob.CustomerSignature = Convert.FromBase64String(workOrderDetail.Signature);
                    objJob.CustomerContactNo = workOrderDetail.ContactNo;
                    objJob.Remarks = workOrderDetail.Remarks;

                    db.Jobs.AddObject(objJob);
                    db.SaveChanges();

                    //Save pdi details
                    SavePdiDetail(workOrderDetail, objJob.JobId);

                    //Save spare part details
                    SaveWorkOrderSpareDetails(workOrderDetail, objJob.JobId);
                }

                

                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.InnerException.Message, JsonRequestBehavior.AllowGet);
            }

            //return Json("Ok", JsonRequestBehavior.AllowGet);
        }

        private void SavePdiDetail(WorkOrderDetailViewModel workOrderDetail, Int64 jobId)
        {
            var PdiDetail = new PDIDetail();
            
                if (ModelState.IsValid)
                {
                    PdiDetail.JobId = jobId;
                    PdiDetail.BarCode = workOrderDetail.BarCode;
                    PdiDetail.RSId = workOrderDetail.RSId;
                    PdiDetail.Lane = workOrderDetail.Lane;
                    PdiDetail.Model = workOrderDetail.Model;
                    PdiDetail.AcOrDc = workOrderDetail.AcOrDc;
                    PdiDetail.WaterSource = workOrderDetail.WaterSource;
                    PdiDetail.ManufecturerName = workOrderDetail.ManufecturerName;
                    PdiDetail.ManufacturerYear = workOrderDetail.ManufecturerYear;
                    PdiDetail.Branding = workOrderDetail.Branding;
                    PdiDetail.OverAllBodyCondition = workOrderDetail.OverAllBodyCondition;
                    db.PDIDetails.AddObject(PdiDetail);
                    db.SaveChanges();
                }
        }
       
        private void SaveComplaintDetail(WorkOrderDetailViewModel workOrderDetail, Int64 jobId)
        {
            List<ComplaintDetailViewModel> complaintList = workOrderDetail.ComplaintDetail;
            if (complaintList != null && complaintList.Count > 0)
            {
                if (ModelState.IsValid)
                {
                    complaintList.ForEach(complaint =>
                    {
                        var jobComplaint = new JobComplaint();
                        jobComplaint.JobId = jobId;
                        jobComplaint.ComplaintCategoryId = complaint.ComplaintCategoryId;
                        jobComplaint.RouteCauseId = complaint.RouteCauseId;
                        db.JobComplaints.AddObject(jobComplaint);
                    });

                    db.SaveChanges();
                }
            }
        }

        private void SaveWorkOrderParameters(WorkOrderDetailViewModel workOrderDetail, Int64 jobId)
        {
            List<WorkOrder> workOrderParameterList = workOrderDetail.WorkOrderParameterDetail;
            if (workOrderParameterList != null && workOrderParameterList.Count > 0)
            {
                if (ModelState.IsValid)
                {
                    workOrderParameterList.ForEach(workOrder =>
                    {
                        if (workOrderDetail.JobId == 0)
                            workOrder.JobId = jobId;
                        db.WorkOrders.AddObject(workOrder);
                    });

                    db.SaveChanges();
                }
            }
        }

        private void SaveWorkOrderSpareDetails(WorkOrderDetailViewModel workOrderDetail, Int64 jobId)
        {
            if (workOrderDetail.IsSparePartsUsed)
            {
                if (workOrderDetail.SparePartUsed != null && workOrderDetail.SparePartUsed.Count > 0)
                {
                    var SparePartList = workOrderDetail.SparePartUsed;
                    SparePartList.ForEach(sparePart =>
                    {
                        var sparePartUsed = new JobInventoryItem()
                        {
                            JobId = jobId,
                            InventoryItemId = sparePart.ItemId,
                            Quantity = sparePart.Quantity,
                        };

                        db.JobInventoryItems.AddObject(sparePartUsed);
                    });

                    db.SaveChanges();
                }
            }
        }

        private void SaveWorkOrderIngredientDetails(WorkOrderDetailViewModel workOrderDetail, Int64 jobId)
        {
            var ingredientList = workOrderDetail.IngredientDetail;
            if (ingredientList.Count > 0)
            {
                ingredientList.ForEach(ingredient =>
                {
                    var ingredientDetail = new JobIngredient()
                    {
                        JobId = jobId,
                        IngredientId = ingredient.IngredientId,
                        Water = ingredient.Water,
                        Premix = ingredient.Premix,
                        Temp = ingredient.Temp,
                        Counter = ingredient.Counter
                    };

                    db.JobIngredients.AddObject(ingredientDetail);
                });
                db.SaveChanges();
            }
            
        }

        private void SaveWorkOrderPictures(WorkOrderDetailViewModel workOrderDetail,Int64 jobId)
        {
            var pictures = workOrderDetail.Pictures;
            if (workOrderDetail.Pictures != null && workOrderDetail.Pictures.Count > 0)
            {
                pictures.ForEach(picture =>
                {
                    var pictureEntry = new JobImage()
                    {
                        JobId = jobId,
                        Image = Convert.FromBase64String(picture)
                    };

                    db.JobImages.AddObject(pictureEntry);
                }
                    );

                db.SaveChanges();
            }
        }
        
        // Convert WorkOrderParameter to WorkOrderParameterViewModel.
        private WorkOrderParameterViewModel ModelToViewModel(WorkOrderParameter model)
        {
            WorkOrderParameterViewModel viewModel = new WorkOrderParameterViewModel();
            viewModel.ParameterName = model.ParameterName;
            viewModel.ParameterType = model.ParameterType;
            viewModel.WorkOrderParameterId = model.WorkOrderParameterId;

            return viewModel;
        }

        #endregion


        public ActionResult GetWorkOrderPicture(int id)
        {
            if (id > 0)
            {
                using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                {
                    byte[] image = db.JobImages.Where(m => m.ImageId == id).SingleOrDefault().Image;

                    // 78 is the size of the OLE header for Northwind images
                    //ms.Write(image, 78, image.Length - 78);

                    return File(image, "image/PNG");
                }
            }
            return null;
        }

        public ActionResult GetWorkOrderSignature(int id)
        {
            if (id > 0)
            {
                using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                {
                    byte[] image = db.Jobs.Where(m => m.JobId == id).SingleOrDefault().CustomerSignature;
                    return image == null?null: File(image, "image/PNG");
                }
            }
            return null;
        }



        public ActionResult Index()
        {
            var workorders = db.WorkOrders.Include("Company").Include("Job").Include("WorkOrderParameter");
            return View(workorders.ToList());
        }

        public ActionResult Details(int JobTypeId, long id = 0)
        {
            string zone = TimeZone.CurrentTimeZone.StandardName;
            var job = db.Jobs.Where(n => n.JobId == id).SingleOrDefault();
            if (JobTypeId == 3 || JobTypeId == 5)
            {
                
                var inventoryDetail = db.GetJobInventoryDetail(id).ToList();
                ViewBag.JobInventoryDetail = db.GetJobInventoryDetail(id).ToList();
                ViewBag.PDIDetail = db.GetPDIDetail(id).SingleOrDefault();
                return View("PDIDetails", job);
            }
            ViewBag.InstallationWorkOrderParameters = db.WorkOrderParameters.Where(c => c.ParameterType != string.Empty).ToList();
            ViewBag.PreventiveWorkOrderParameters = db.WorkOrderParameters.Where(c => c.ParameterType == string.Empty).ToList();
            ViewBag.JobIngredientDetail = db.GetJobIngredientDetail(id).ToList();
            ViewBag.JobInventoryDetail = db.GetJobInventoryDetail(id).ToList();
            ViewBag.JobImages = db.JobImages.Where(n => n.JobId == id).ToList();
            ViewBag.JobComplaintDetail = db.GetJobComplaintDetails(id).ToList();
            return View(job);
        }

        public ActionResult Create()
        {
            ViewBag.CompanyId = new SelectList(db.Companies, "CompanyId", "CompanyName");
            ViewBag.JobId = new SelectList(db.Jobs, "JobId", "Type");
            ViewBag.WorkOrderParameterId = new SelectList(db.WorkOrderParameters, "WorkOrderParameterId", "ParameterName");
            return View();
        }

        //
        // POST: /WorkOrder/Create

        [HttpPost]
        public ActionResult Create(WorkOrder workorder)
        {
            if (ModelState.IsValid)
            {
                db.WorkOrders.AddObject(workorder);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CompanyId = new SelectList(db.Companies, "CompanyId", "CompanyName", workorder.CompanyId);
            ViewBag.JobId = new SelectList(db.Jobs, "JobId", "Type", workorder.JobId);
            ViewBag.WorkOrderParameterId = new SelectList(db.WorkOrderParameters, "WorkOrderParameterId", "ParameterName", workorder.WorkOrderParameterId);
            return View(workorder);
        }

        //
        // GET: /WorkOrder/Edit/5

        public ActionResult Edit(long id = 0)
        {
            WorkOrder workorder = db.WorkOrders.Single(w => w.WorkOrderId == id);
            if (workorder == null)
            {
                return HttpNotFound();
            }
            ViewBag.CompanyId = new SelectList(db.Companies, "CompanyId", "CompanyName", workorder.CompanyId);
            ViewBag.JobId = new SelectList(db.Jobs, "JobId", "Type", workorder.JobId);
            ViewBag.WorkOrderParameterId = new SelectList(db.WorkOrderParameters, "WorkOrderParameterId", "ParameterName", workorder.WorkOrderParameterId);
            return View(workorder);
        }

        //
        // POST: /WorkOrder/Edit/5

        [HttpPost]
        public ActionResult Edit(WorkOrder workorder)
        {
            if (ModelState.IsValid)
            {
                db.WorkOrders.Attach(workorder);
                db.ObjectStateManager.ChangeObjectState(workorder, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CompanyId = new SelectList(db.Companies, "CompanyId", "CompanyName", workorder.CompanyId);
            ViewBag.JobId = new SelectList(db.Jobs, "JobId", "Type", workorder.JobId);
            ViewBag.WorkOrderParameterId = new SelectList(db.WorkOrderParameters, "WorkOrderParameterId", "ParameterName", workorder.WorkOrderParameterId);
            return View(workorder);
        }

        //
        // GET: /WorkOrder/Delete/5

        public ActionResult Delete(long id = 0)
        {
            WorkOrder workorder = db.WorkOrders.Single(w => w.WorkOrderId == id);
            if (workorder == null)
            {
                return HttpNotFound();
            }
            return View(workorder);
        }

        //
        // POST: /WorkOrder/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(long id)
        {
            WorkOrder workorder = db.WorkOrders.Single(w => w.WorkOrderId == id);
            db.WorkOrders.DeleteObject(workorder);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}