﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VendingInspection.Models;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;

namespace VendingInspection.Controllers
{
    public class RouteCauseController : Controller
    {
        private VendingCareEntities db = new VendingCareEntities();


        #region Method and Functions related to Web Service


        [HttpGet]
        public ActionResult GetComplaintCategories()
        {
            try
            {
                List<ComplaintCategory> compaintCategories = db.ComplaintCategories.ToList();
                if (ModelState.IsValid)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    string result = serializer.Serialize(compaintCategories);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        [HttpGet]
        public ActionResult GetRouteCause()
        {
            try
            {
                
                var routeCauses = db.RouteCauses.Select(routeCause => 
                    new {
                        RouteCauseId = routeCause.RouteCauseId,
                        RouteCauseName = routeCause.RouteCause1,
                        ComplaintCategoryId = routeCause.ComplaintCategoryId
                    });
                if (ModelState.IsValid)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    string result = serializer.Serialize(routeCauses);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }

            return null;
        }


        #endregion



        //
        // GET: /RouteCause/

        public ActionResult Index()
        {
            var routecauses = db.RouteCauses.Include("ComplaintCategory");
            return View(routecauses.ToList());
        }

        //
        // GET: /RouteCause/Details/5

        public ActionResult Details(int id = 0)
        {
            RouteCause routecause = db.RouteCauses.Single(r => r.RouteCauseId == id);
            if (routecause == null)
            {
                return HttpNotFound();
            }
            return View(routecause);
        }

        //
        // GET: /RouteCause/Create

        public ActionResult Create()
        {
            ViewBag.ComplaintCategoryId = new SelectList(db.ComplaintCategories, "ComplaintCategoryId", "ComplaintCategory1");
            return View();
        }

        //
        // POST: /RouteCause/Create

        [HttpPost]
        public ActionResult Create(RouteCause routecause)
        {
            if (ModelState.IsValid)
            {
                var routeCause = routecause.RouteCause1;
                routeCause = Regex.Replace(routeCause, @"\t|\n|\r", "");
                routecause.RouteCause1 = routeCause;
                db.RouteCauses.AddObject(routecause);
                db.SaveChanges();
                return View(routecause);
            }

            ViewBag.ComplaintCategoryId = new SelectList(db.ComplaintCategories, "ComplaintCategoryId", "ComplaintCategory1", routecause.ComplaintCategoryId);
            return View(routecause);
        }

        //
        // GET: /RouteCause/Edit/5

        public ActionResult Edit(int id = 0)
        {
            RouteCause routecause = db.RouteCauses.Single(r => r.RouteCauseId == id);
            if (routecause == null)
            {
                return HttpNotFound();
            }
            ViewBag.ComplaintCategoryId = new SelectList(db.ComplaintCategories, "ComplaintCategoryId", "ComplaintCategory1", routecause.ComplaintCategoryId);
            return View(routecause);
        }

        //
        // POST: /RouteCause/Edit/5

        [HttpPost]
        public ActionResult Edit(RouteCause routecause)
        {
            if (ModelState.IsValid)
            {
                db.RouteCauses.Attach(routecause);
                db.ObjectStateManager.ChangeObjectState(routecause, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ComplaintCategoryId = new SelectList(db.ComplaintCategories, "ComplaintCategoryId", "ComplaintCategory1", routecause.ComplaintCategoryId);
            return View(routecause);
        }

        //
        // GET: /RouteCause/Delete/5

        public ActionResult Delete(int id = 0)
        {
            RouteCause routecause = db.RouteCauses.Single(r => r.RouteCauseId == id);
            if (routecause == null)
            {
                return HttpNotFound();
            }
            return View(routecause);
        }

        //
        // POST: /RouteCause/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            RouteCause routecause = db.RouteCauses.Single(r => r.RouteCauseId == id);
            db.RouteCauses.DeleteObject(routecause);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}