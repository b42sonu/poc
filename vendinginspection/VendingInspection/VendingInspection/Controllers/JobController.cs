﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VendingInspection.Models;
using VendingInspection.ViewModels;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;


namespace VendingInspection.Controllers
{
    public class JobController : Controller
    {
        private VendingCareEntities db = new VendingCareEntities();

        #region Methods and Functions related to Web Service
        // Get user jobs
        [HttpGet]
        public ActionResult GetUserJobs(string userDetailId)
        {
            try
            {
                List<UserJobInformation> userJobInformation = db.GetUserJobs(Convert.ToInt64(userDetailId)).ToList();
                List<UserJobInformationViewModel> userDetailsViewModel = new List<UserJobInformationViewModel>();
                if (ModelState.IsValid && userJobInformation != null)
                {
                    userJobInformation.ForEach(jobInformation =>
                        {
                            userDetailsViewModel.Add(ModelToViewModel(jobInformation));

                        });

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    string result = serializer.Serialize(userDetailsViewModel);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        [HttpPost]
        public ActionResult SetJobStartTime(string value)
        {
            try
            {
                var data = (JObject)JsonConvert.DeserializeObject(value);
                //Set job finish time
                var jobId = data["jobId"].ToObject<Int64>();
                var currentLocation = data["currentLocation"].ToObject<String>();
                Job job = db.Jobs.Single(j => j.JobId == jobId);
                job.JobStartDate = DateTime.Now;
                job.JobStartLocation = currentLocation;
                db.ObjectStateManager.ChangeObjectState(job, EntityState.Modified);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(ex.InnerException.Message, JsonRequestBehavior.AllowGet);
            }

            return Json("Ok", JsonRequestBehavior.AllowGet);
        }

        // Convert UserJobInformation to UserJobInformationViewModel.
        private UserJobInformationViewModel ModelToViewModel(UserJobInformation model)
        {
            UserJobInformationViewModel viewModel = new UserJobInformationViewModel();
            viewModel.JobId = model.JobId;
            viewModel.JobTypeId = model.JobTypeId;
            viewModel.BarCode = model.BarCode;
            viewModel.CustomerName = model.CustomerName;
            viewModel.CustomerAddress = model.CustomerAddress;
            viewModel.ContactPerson = model.ContactPerson;
            viewModel.ContactNumber = model.ContactNumber;
            viewModel.MachineLocation = model.MachineLocation;
            viewModel.Lane = model.Lane;
            viewModel.Model = model.Model;
            viewModel.AcOrDc = model.AcOrDc;
            viewModel.WaterSource = model.WaterSource;
            viewModel.ManufacturerName = model.ManufacturerName;
            viewModel.ManufacturerYear = model.ManufacturerYear;
            viewModel.Branding = model.Branding;
            return viewModel;
        }
        #endregion
        //
        // GET: /Job/

        public ActionResult Index()
        {
            List<JobInformation> jobs = db.GetAllJobs().ToList<JobInformation>();
            return View(jobs.ToList());
        }

        //
        // GET: /Job/Details/5

        public ActionResult Details(long id = 0)
        {
            var job = db.GetAllJobs().Where(j => j.JobId == id).SingleOrDefault();
            if (job == null)
            {
                return HttpNotFound();
            }
            return View(job);
        }

        //
        // GET: /Job/Create

        public ActionResult Create()
        {
            ViewBag.Technicians = new SelectList(db.GetAllTechnicians(), "UserId", "FirstName");
            ViewBag.CallCategories = new SelectList(db.ComplaintCategories, "ComplaintCategoryId","ComplaintCategory1");
            ViewBag.JobTypes = new SelectList(db.JobTypes.Where(c => c.JobTypeId != 3 && c.JobTypeId != 5), "JobTypeId", "Description");
            return View();
        }

        //
        // POST: /Job/Create

        [HttpPost]
        public ActionResult Create(Job job)
        {
            if (ModelState.IsValid)
            {
                job.CompanyId = (Int32)Session["CompanyId"];
                job.JobCreationDate = DateTime.Now;
                job.Status = 0;
                db.Jobs.AddObject(job);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CompanyId = new SelectList(db.Companies, "CompanyId", "CompanyName", job.CompanyId);
            return View(job);
        }

        //
        // GET: /Job/Edit/5

        public ActionResult Edit(long id = 0)
        {
            Job job = db.Jobs.Single(j => j.JobId == id);
            ViewBag.Technicians = new SelectList(db.GetAllTechnicians(), "UserId", "FirstName", job.Technician);
            ViewBag.CallCategories = new SelectList(db.ComplaintCategories, "ComplaintCategoryId", "ComplaintCategory1");
            ViewBag.JobTypes = new SelectList(db.JobTypes.Where(c => c.JobTypeId != 3 && c.JobTypeId != 5), "JobTypeId", "Description",job.JobTypeId);
            if (job == null)
            {
                return HttpNotFound();
            }
            ViewBag.Technicians = new SelectList(db.GetAllTechnicians(), "UserId", "FirstName");
            return View(job);
        }

        //
        // POST: /Job/Edit/5

        [HttpPost]
        public ActionResult Edit(Job job)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    var jobToUpdate = db.Jobs.SingleOrDefault(j => j.JobId == job.JobId);
                    jobToUpdate.Technician = job.Technician;
                    jobToUpdate.JobTypeId = job.JobTypeId;
                    jobToUpdate.RegisteredByName = job.RegisteredByName;
                    jobToUpdate.RegisteredByNumber = job.RegisteredByNumber;
                    jobToUpdate.CallCategory = job.CallCategory;
                    db.ObjectStateManager.ChangeObjectState(jobToUpdate, EntityState.Modified);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                HandleErrorInfo errorInfo = new HandleErrorInfo(ex, "Job", "Edit");
                return View(job);
            }
            ViewBag.Technicians = new SelectList(db.GetAllTechnicians(), "UserId", "FirstName");
            return View(job);
        }

        //
        // GET: /Job/Delete/5

        public ActionResult Delete(long id = 0)
        {
            Job job = db.Jobs.Single(j => j.JobId == id);
            db.Jobs.DeleteObject(job);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}