﻿using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using VendingInspection.Models;
using System.Web.Script.Serialization;
using VendingInspection.ViewModels;
using WebMatrix.WebData;

namespace VendingInspection.Controllers
{
    public class UserController : Controller
    {
        private VendingCareEntities db = new VendingCareEntities();


        #region Methods and Functions related to Web Service

        


        // Login for Vending care
        [AllowAnonymous]
        [HttpGet]
        public ActionResult LoginVendingCare(string username, string password)
        {
            try
            {
                var userDetailId = db.AuthenticateUser(username, password).FirstOrDefault();
                if (ModelState.IsValid && userDetailId.HasValue && userDetailId.Value > 0)
                {
                    return Json(userDetailId.ToString(), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        // Get user details
        [HttpGet]
        public ActionResult GetUserDetails(string userDetailId)
        {
            try
            {
                UserDetail userDetail = db.GetUserDetail(Convert.ToInt64(userDetailId)).FirstOrDefault();

                if (ModelState.IsValid && userDetail != null)
                {
                    UserDetailsViewModel userDetailsViewModel = ModelToViewModel(userDetail);
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    string result = serializer.Serialize(userDetailsViewModel);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        // Convert UserDetail to UserDetailsViewModel.
        private UserDetailsViewModel ModelToViewModel(UserDetail model)
        {
            UserDetailsViewModel viewModel = new UserDetailsViewModel();
            viewModel.UserId = model.UserId;
            viewModel.UserDetailId = model.UserDetailId;
            viewModel.CompanyId = Convert.ToInt32(model.CompanyId);
            viewModel.UserName = model.UserName;
            viewModel.Password = model.Password;
            viewModel.City = model.City;
            viewModel.State = model.State;
            viewModel.ZipCode = model.ZipCode;

            return viewModel;
        }

        #endregion

        //
        // GET: /User/

        public ActionResult Index()
        {
            var userdetails = db.UserDetails.Include("Company");
            return View(userdetails.ToList());
        }

        //
        // GET: /User/Details/5

        public ActionResult Details(long id = 0)
        {
            UserDetail userdetail = db.UserDetails.Single(u => u.UserDetailId == id);
            if (userdetail == null)
            {
                return HttpNotFound();
            }
            return View(userdetail);
        }

        //
        // GET: /User/Create

        public ActionResult Create()
        {
            ViewBag.Roles = new SelectList(Roles.GetAllRoles());
            return View();
        }

        //
        // POST: /User/Create

        [HttpPost]
        public ActionResult Create(UserDetail userdetail, string Role)
        {
            //userdetail.UserId = 0;

            if (ModelState.IsValid)
            {
                try
                {

                    if (WebSecurity.UserExists(userdetail.UserName))
                    {
                        ModelState.AddModelError("", ErrorMessges.ErrorCodeToString(MembershipCreateStatus.DuplicateUserName));
                    }
                    else
                    {
                        WebSecurity.CreateUserAndAccount(userdetail.UserName, userdetail.Password);
                        Roles.AddUserToRole(userdetail.UserName, Role);
                        int userId = WebSecurity.GetUserId(userdetail.UserName);
                        userdetail.UserId = userId;
                        userdetail.CompanyId =(Int32) Session["CompanyId"];
                        db.UserDetails.AddObject(userdetail);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorMessges.ErrorCodeToString(e.StatusCode));
                }
                // If we got this far, something failed, redisplay form
                ViewBag.Roles = new SelectList(Roles.GetAllRoles());
                return View(userdetail);
            }
            ViewBag.CompanyId = new SelectList(db.Companies, "CompanyId", "CompanyName", userdetail.CompanyId);
            return View(userdetail);
        }

        //
        // GET: /User/Edit/5

        public ActionResult Edit(long id = 0)
        {
            UserDetail userdetail = db.UserDetails.Single(u => u.UserDetailId == id);
            var roleName = Roles.GetRolesForUser(userdetail.UserName);
            ViewBag.Roles = new SelectList(Roles.GetAllRoles(), roleName[0]);
            if (userdetail == null)
            {
                return HttpNotFound();
            }
            ViewBag.CompanyId = new SelectList(db.Companies, "CompanyId", "CompanyName", userdetail.CompanyId);
            return View(userdetail);
        }

        //
        // POST: /User/Edit/5

        [HttpPost]
        public ActionResult Edit(UserDetail userdetail,string Role)
        {
            if (ModelState.IsValid)
            {
                var userFromDB = db.GetUserDetail(userdetail.UserDetailId).SingleOrDefault();
                userFromDB.UserName = userdetail.UserName;
                userFromDB.FirstName = userdetail.FirstName;
                userFromDB.LastName = userdetail.LastName;
                userFromDB.Address = userdetail.Address;
                userFromDB.City = userdetail.City;
                userFromDB.State = userdetail.State;
                userFromDB.ZipCode = userdetail.ZipCode;
                userFromDB.Region = userdetail.Region;
                userFromDB.ServiceProviderName = userdetail.ServiceProviderName;
                userFromDB.Designation = userdetail.Designation;
                userFromDB.Age = userdetail.Age;
                userFromDB.MobileNumber = userdetail.MobileNumber;
                if(userdetail.Password != userFromDB.Password)
                {
                    userFromDB.Password = userdetail.Password;
                    WebSecurity.ChangePassword(userFromDB.UserName, userFromDB.Password, userdetail.Password);
                }

                var roleName = Roles.GetRolesForUser(userdetail.UserName);
                if (!roleName.Contains(Role))
                {
                    Roles.RemoveUserFromRole(userdetail.UserName, roleName[0]);
                    Roles.AddUserToRole(userdetail.UserName,Role);
                }

                db.ObjectStateManager.ChangeObjectState(userFromDB, EntityState.Modified);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            ViewBag.CompanyId = new SelectList(db.Companies, "CompanyId", "CompanyName", userdetail.CompanyId);
            return View(userdetail);
        }

        //
        // GET: /User/Delete/5

        public ActionResult Delete(long id = 0)
        {
            UserDetail userdetail = db.UserDetails.Single(u => u.UserDetailId == id);
            if (userdetail == null)
            {
                return HttpNotFound();
            }
            return View(userdetail);
        }

        //
        // POST: /User/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(long id)
        {
            UserDetail userdetail = db.UserDetails.Single(u => u.UserDetailId == id);
            db.UserDetails.DeleteObject(userdetail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}