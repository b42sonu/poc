﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VendingInspection.ViewModels;
using VendingInspection.Models;
using System.Web.Script.Serialization;
using System.Web.Mvc;
namespace VendingInspection.Controllers
{
    public class WebAPIController : Controller
    {
        private VendingCareEntities db = new VendingCareEntities();
        // GET api/webapi
        public JsonResult GetMasterData()
        {
            var masterData = new MasterDataViewModel();
            masterData.RouteCauses = GetRouteCauses();
            masterData.Brands = GetBrands();
            masterData.RSDetails = GetRS();
            masterData.Manufacturers = GetManufacturers();
            masterData.ComplaintCategories = GetComplaintCategories();
            masterData.Models = GetModels();
            masterData.WaterSources = GetWaterSources();
            masterData.Lanes = GetLanes();
            masterData.ManufacturerYears = GetManufacturerYears();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string result = serializer.Serialize(masterData);
            return Json(result, JsonRequestBehavior.AllowGet);
            
        }

        private List<object> GetComplaintCategories()
        {
            try
            {

                var complaintCategories = db.ComplaintCategories.Select(category =>
                    new
                    {
                        ComplaintCategoryId = category.ComplaintCategoryId,
                        ComplaintCategoryName = category.ComplaintCategory1,
                        
                    });
                return complaintCategories.ToList<Object>();
            }
            catch (Exception ex)
            {

            }

            return null;
        }


        private List<object> GetRouteCauses()
        {
            try
            {

                var routeCauses = db.RouteCauses.Select(routeCause =>
                    new
                    {
                        RouteCauseId = routeCause.RouteCauseId,
                        RouteCauseName = routeCause.RouteCause1,
                        ComplaintCategoryId = routeCause.ComplaintCategoryId
                    });
                return routeCauses.ToList<Object>();
            }
            catch (Exception ex)
            {
                
            }

            return null;
        }

        private List<object> GetBrands()
        {
            var brands = new List<object>();
            foreach (var brand in db.Brands)
            {
                brands.Add(brand.BrandName);
            }
            return brands;
        }

        private List<object> GetModels()
        {
            var models = new List<object>();
            foreach (var model in db.Models)
            {
                models.Add(model.ModelName);
            }
            return models;
        }

        private List<object> GetWaterSources()
        {
            var waterSources = new List<object>();
            foreach (var waterSource in db.WaterSources)
            {
                waterSources.Add(waterSource.WaterSourceName);
            }
            return waterSources;
        }

        private List<object> GetLanes()
        {
            var lanes = new List<object>();
            lanes.Add("1-Lane");
            lanes.Add("2-Lanes");
            lanes.Add("3-Lanes");
            lanes.Add("4-Lanes");
            lanes.Add("5-Lanes");
            lanes.Add("6-Lanes");
            return lanes;
        }

        private List<object> GetManufacturerYears()
        {
            var years = new List<object>();
            for (int i = 0; i <= 20; i++)
            {
                if (i < 9)
                    years.Add("200" + i);
                else
                    years.Add("20" + i);
            }
            return years;
        }

        private List<object> GetManufacturers()
        {
            var manufacturers = new List<object>();
            foreach (var manufacturer in db.Manufacturers)
            {
                manufacturers.Add(manufacturer.MfgrName);
            }
            return manufacturers;
        }

        private List<object> GetRS()
        {
            var rsDetails = db.RSDetails.Select(rsDetail => new
                                                {
                                                    RSId = rsDetail.RSId,
                                                    RSName = rsDetail.RSName

                                                }).ToList<object>();
            return rsDetails;
        }


       

        
    }
}
