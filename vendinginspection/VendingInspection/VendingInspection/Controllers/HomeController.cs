﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VendingInspection.Models;

namespace VendingInspection.Controllers
{
    public class HomeController : Controller
    {
        private VendingCareEntities db = new VendingCareEntities();
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        public ActionResult DashBoard()
        {
            var technicianStatus = db.GetTechnicianStatus().ToList();
            return View(technicianStatus);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
