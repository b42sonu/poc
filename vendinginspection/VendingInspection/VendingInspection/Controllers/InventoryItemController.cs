﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VendingInspection.Models;
using System.Web.Script.Serialization;
using VendingInspection.ViewModels;
using System.Text.RegularExpressions;

namespace VendingInspection.Controllers
{
    public class InventoryItemController : Controller
    {
        private VendingCareEntities db = new VendingCareEntities();

        #region Webservice related methods and functions

        [HttpGet]
        public ActionResult GetInventoryItems()
        {
            try
            {

                List<InventoryItem> inventoryItems = db.InventoryItems.Where(c => c.ItemId < 157).ToList();
                List<InventoryItemViewModel> inventory = new List<InventoryItemViewModel>();
                if (ModelState.IsValid)
                {
                    inventoryItems.ForEach(inventoryItem => inventory.Add(new InventoryItemViewModel()
                    {
                        ItemId = inventoryItem.ItemId,
                        ItemName = inventoryItem.ItemName,
                        Quantity = inventoryItem.Quantity
                    }));

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    string result = serializer.Serialize(inventory);
                    return Json(result, "application/json; charset=utf-8",JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }

            return null;
        }


        #endregion


        //
        // GET: /InventoryItem/

        public ActionResult Index()
        {
            return View(db.InventoryItems.ToList());
        }

        //
        // GET: /InventoryItem/Details/5

        public ActionResult Details(int id = 0)
        {
            InventoryItem inventoryitem = db.InventoryItems.Single(i => i.ItemId == id);
            if (inventoryitem == null)
            {
                return HttpNotFound();
            }
            return View(inventoryitem);
        }

        //
        // GET: /InventoryItem/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /InventoryItem/Create

        [HttpPost]
        public ActionResult Create(InventoryItem inventoryitem)
        {
            if (ModelState.IsValid)
            {
                var inventoryItemName = inventoryitem.ItemName;
                inventoryItemName = Regex.Replace(inventoryItemName, @"\t|\n|\r", "");
                inventoryitem.ItemName = inventoryItemName;
                db.InventoryItems.AddObject(inventoryitem);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(inventoryitem);
        }

        //
        // GET: /InventoryItem/Edit/5

        public ActionResult Edit(int id = 0)
        {
            InventoryItem inventoryitem = db.InventoryItems.Single(i => i.ItemId == id);
            if (inventoryitem == null)
            {
                return HttpNotFound();
            }
            return View(inventoryitem);
        }

        //
        // POST: /InventoryItem/Edit/5

        [HttpPost]
        public ActionResult Edit(InventoryItem inventoryitem)
        {
            if (ModelState.IsValid)
            {
                db.InventoryItems.Attach(inventoryitem);
                db.ObjectStateManager.ChangeObjectState(inventoryitem, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(inventoryitem);
        }

        //
        // GET: /InventoryItem/Delete/5

        public ActionResult Delete(int id = 0)
        {
            InventoryItem inventoryitem = db.InventoryItems.Single(i => i.ItemId == id);
            if (inventoryitem == null)
            {
                return HttpNotFound();
            }
            return View(inventoryitem);
        }

        //
        // POST: /InventoryItem/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            InventoryItem inventoryitem = db.InventoryItems.Single(i => i.ItemId == id);
            db.InventoryItems.DeleteObject(inventoryitem);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}