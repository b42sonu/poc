﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VendingInspection.Models;

namespace VendingInspection.Controllers
{
    public class UserInventoryItemController : Controller
    {
        private VendingCareEntities db = new VendingCareEntities();

        //
        // GET: /UserInventoryItem/

        public ActionResult Index(long id = 0)
        {
            ViewBag.Message = TempData["Message"];
            IEnumerable<UserInventoryItemsForUserId> list = null;
            if (id > 0)
            {
                var userinventoryitems = db.GetUserInventoryItems(id);
                    //db.UserInventoryItems.Include("InventoryItem").Include("UserDetail");
                list = userinventoryitems.ToList();
            }
            return View(list);
        }

        //
        // GET: /UserInventoryItem/Details/5

        public ActionResult Details(long id = 0)
        {
            UserInventoryItem userinventoryitem = db.UserInventoryItems.Single(u => u.UserInventoryItemId == id);
            if (userinventoryitem == null)
            {
                return HttpNotFound();
            }
            return View(userinventoryitem);
        }

        //
        // GET: /UserInventoryItem/Create

        public ActionResult Create()
        {
            ViewBag.ItemId = new SelectList(db.InventoryItems, "ItemId", "ItemName");
            ViewBag.UserDetailId = new SelectList(db.UserDetails, "UserDetailId", "UserName");
            return View();
        }

        //
        // POST: /UserInventoryItem/Create

        [HttpPost]
        public ActionResult Create(UserInventoryItem userinventoryitem)
        {
            if (ModelState.IsValid)
            {
                db.UserInventoryItems.AddObject(userinventoryitem);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ItemId = new SelectList(db.InventoryItems, "ItemId", "ItemName", userinventoryitem.ItemId);
            ViewBag.UserDetailId = new SelectList(db.UserDetails, "UserDetailId", "UserName", userinventoryitem.UserDetailId);
            return View(userinventoryitem);
        }

        //
        // GET: /UserInventoryItem/Edit/5

        public ActionResult Edit(long userid = 0, long itemId = 0)
        {
            /*UserInventoryItem userinventoryitem = db.UserInventoryItems.Single(u => u.UserInventoryItemId == id);
            if (userinventoryitem == null)
            {
                return HttpNotFound();
            }
            ViewBag.ItemId = new SelectList(db.InventoryItems, "ItemId", "ItemName", userinventoryitem.ItemId);
            ViewBag.UserDetailId = new SelectList(db.UserDetails, "UserDetailId", "UserName", userinventoryitem.UserDetailId);
            return View(userinventoryitem);*/

            var userinventoryitems = db.GetUserInventoryItems(userid).ToList();
            UserInventoryItemsForUserId userInventoryItemsForUserId = userinventoryitems.FirstOrDefault(n => n.ItemId == itemId);
            return View(userInventoryItemsForUserId);
        }

        //
        // POST: /UserInventoryItem/Edit/5

        [HttpPost]
        public ActionResult Edit(UserInventoryItemsForUserId userInventoryItemsForUserId)
        {
            if (userInventoryItemsForUserId.UserDetailId != null)
            {
                UserInventoryItem userinventoryitem = new UserInventoryItem()
                                                          {
                                                              ItemId = userInventoryItemsForUserId.ItemId,
                                                              UserDetailId =
                                                                  (long)userInventoryItemsForUserId.UserDetailId,
                                                              UserInventoryItemId =
                                                                  userInventoryItemsForUserId.UserInventoryItemId,
                                                              Quantity = userInventoryItemsForUserId.Quantity,
                                                          };
                if (ModelState.IsValid)
                {
                    if (userinventoryitem.UserInventoryItemId > 0)
                    {
                        db.UserInventoryItems.Attach(userinventoryitem);
                        db.ObjectStateManager.ChangeObjectState(userinventoryitem, EntityState.Modified);
                        db.SaveChanges();

                    }
                    else
                    {
                        db.UserInventoryItems.AddObject(userinventoryitem);
                        db.SaveChanges();
                    }
                    TempData["Message"] = CommanUtility.GetMessage(CommanUtility.ErrorType.Information, "Record Updated");
                    return RedirectToAction("Index", new { id = userInventoryItemsForUserId.UserDetailId });
                }
                /*ViewBag.ItemId = new SelectList(db.InventoryItems, "ItemId", "ItemName", userinventoryitem.ItemId);
                ViewBag.UserDetailId = new SelectList(db.UserDetails, "UserDetailId", "UserName", userinventoryitem.UserDetailId);
                return View(userinventoryitem);*/
            }
            return RedirectToAction("Index");
        }

        //
        // GET: /UserInventoryItem/Delete/5

        public ActionResult Delete(long id = 0)
        {
            UserInventoryItem userinventoryitem = db.UserInventoryItems.Single(u => u.UserInventoryItemId == id);
            if (userinventoryitem == null)
            {
                return HttpNotFound();
            }
            return View(userinventoryitem);
        }

        //
        // POST: /UserInventoryItem/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(long id)
        {
            UserInventoryItem userinventoryitem = db.UserInventoryItems.Single(u => u.UserInventoryItemId == id);
            db.UserInventoryItems.DeleteObject(userinventoryitem);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}